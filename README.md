# README #

This is a VNG's assignment - a small First Person Shooter game

-----------------------------------------
                        Fast Boy FPS: Road To Hero
Gameplay is fairly simple: Your mission is to rescue the residents trapped in a city where a zombie epidemic is raging.                      
                        
The game is aimed at both mobile and desktop platforms. 
For mobile platform, you can use buttons to take actions of your character and a joystick for the movement, hold a touch at right side and move to rotate character.
For desktop platform, your mouse will navigate the viewing angle, use W/A/S/D key to move, space/B/N to take character's actions and click mouse to fire. 

The game was developed in a very short time, so it is inevitable that there may be many shortcomings but I hope that you will enjoy it.
