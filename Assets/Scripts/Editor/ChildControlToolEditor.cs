using FastBoyFPS.Utils;
using UnityEditor;
using UnityEngine;

namespace FastboyFSP.Utils {
    [CustomEditor(typeof(ChildControlTool))]
    public class ChildControlToolEditor : Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            ChildControlTool a = (ChildControlTool)target;
            if (GUILayout.Button("Reverse")) {
                a.Reverse();
            }
            if (GUILayout.Button("Set Name")) {
                a.SetName();
            }
            if (GUILayout.Button("Group")) {
                a.Group();
            }
            if (GUILayout.Button("Add Collider In Children")) {
                a.AddColliderInChildren();
            }
            if (GUILayout.Button("Modify Collider")) {
                a.ModifyCollider();
            }
        }
    }
}