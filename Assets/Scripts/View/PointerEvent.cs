using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace FastBoyFPS.View {
    public class PointerEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

        #region Properties
        public bool Hold { get; private set; }
        #endregion

        #region Events
        [HideInInspector] public UnityEvent OnPress = new UnityEvent();
        [HideInInspector] public UnityEvent OnEscape = new UnityEvent();
        #endregion

        #region Methods
        public void OnPointerDown(PointerEventData eventData) {
            Hold = true;
            OnPress.Invoke();
        }
        public void OnPointerUp(PointerEventData eventData) {
            Hold = false;
            OnEscape.Invoke();
        }
        public void OnPointerExit(PointerEventData eventData) {
            Hold = false;
            OnEscape.Invoke(); 
        }
        #endregion
    }

}