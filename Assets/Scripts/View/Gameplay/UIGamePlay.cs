using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FastBoyFPS.View {
    public class UIGamePlay : OverridableMonoBehaviour {
        [SerializeField] private Text txtRescue;
        [SerializeField] private Text txtKill;
        [SerializeField] private GameObject popupContainer;

        #region Methods
        private void Start() {
            PopupBase.PopupContainer = popupContainer;
            PopupBase.PreloadPopups();
        }
        protected override void OnEnable() {
            base.OnEnable();
            eventManager.AddListener(Manager.EventID.SurvivorRescued, UpdateRescue);
            eventManager.AddListener(Manager.EventID.ZombieKilled, UpdateKill);
        }
        protected override void OnDisable() {
            base.OnDisable();
            eventManager.RemoveListener(Manager.EventID.SurvivorRescued, UpdateRescue);
            eventManager.RemoveListener(Manager.EventID.ZombieKilled, UpdateKill);
        }

        private void UpdateRescue() {
            dataManager.RescueAmount++;
            txtRescue.text = dataManager.RescueAmount.ToString();
        }

        private void UpdateKill() {
            dataManager.KillAmount++;
            txtKill.text = dataManager.KillAmount.ToString();
        }
        #endregion
    }
}