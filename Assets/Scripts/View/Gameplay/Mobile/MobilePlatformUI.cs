using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Manager;
using FastBoyFPS.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace FastBoyFPS.View {
    public class MobilePlatformUI : UIBase {
        [SerializeField] private PointerEvent btnShoot;
        [SerializeField] private PointerEvent btnCut;
        [SerializeField] private PointerEvent btnThrow;
        [SerializeField] private Button btnRescue;

        #region Methods
        private void Start() {
            btnCut.OnPress.AddListener(OnButtonCut);
            btnThrow.OnPress.AddListener(OnButtonThrow);
        }
        public override void OnUpdate() {
            if (PlatformManager.targetPlatform == TargetPlatform.Mobile) {
                if (btnShoot.Hold) eventManager.DispatcherListener(Manager.EventID.PlayerShoot);
                else eventManager.DispatcherListener(Manager.EventID.PlayerStopShoot);
            }
        }
        public void OnButtonCut() {
            CustomizedDebug.Log("Player cut");
            eventManager.DispatcherListener(Manager.EventID.PlayerCut);
        }
        public void OnButtonThrow() {
            eventManager.DispatcherListener(Manager.EventID.PlayerThrow);
        }
        public void OnButtonRescue() {
            eventManager.DispatcherListener(Manager.EventID.PlayerRescue);
        }
        #endregion
    }
}