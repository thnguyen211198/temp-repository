using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FastBoyFPS.View {
    public class UIHome : UIBase {
        [SerializeField] private Button btnStart;
        [SerializeField] private GameObject loadingPanel;
        [SerializeField] private Image imgLoadingBar;
        [SerializeField] private Text txtLoadingProcess;

        #region Methods
        private void Start() {
            
        }

        public void OnButtonStart() {
            btnStart.gameObject.SetActive(false);
            loadingPanel.SetActive(true);
            StartCoroutine(Loading());
        }

        private IEnumerator Loading() {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(2);
            while (!asyncLoad.isDone) {
                imgLoadingBar.fillAmount = asyncLoad.progress;
                txtLoadingProcess.text = ((int)asyncLoad.progress).ToString() + "%";
                yield return new WaitForSeconds(0.2f);
            }
        }
        #endregion
    }
}

