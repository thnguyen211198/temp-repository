using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FastBoyFPS.View {
    public class PopupGameOver : PopupBase<PopupGameOver> {
        [SerializeField] private Text txtRescurePoint;
        [SerializeField] private Text txtKillPoint;
        [SerializeField] private Text txtTotalTime;
        [SerializeField] private Text txtScore;

        #region Methods
        protected override void OnEnable() {
            base.OnEnable();
            UpdateScore();
        }
        private void UpdateScore() {
            txtRescurePoint.text = dataManager.RescueAmount.ToString();
            txtKillPoint.text = dataManager.KillAmount.ToString();
            int t = (int)dataManager.SurvivalTime;
            int min = t / 60;
            int seconds = t - min * 60;
            txtTotalTime.text = $"{min:00}:{seconds:00}";
            txtScore.text = ((int)(dataManager.RescueAmount * 3 + dataManager.KillAmount + dataManager.SurvivalTime / 30)).ToString(); 
        }
        public void OnButtonRestart() {
            SceneManager.LoadScene(2);
        }
        public void OnButtonHome() {
            SceneManager.LoadScene(0);
        }
        #endregion
    }
}