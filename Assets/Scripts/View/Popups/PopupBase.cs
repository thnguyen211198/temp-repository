using System;
using System.Collections.Generic;
using FastBoyFPS.Manager;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.View {
    public struct PopupPath {
        public static readonly Dictionary<string, string> Dictionary = new Dictionary<string, string> {
                {"PopupGameOver", "Prefabs/View/PopupGameOver"}
            };
    }
    public abstract class PopupBase : UIBase {
        #region Property
        public static GameObject PopupContainer { get; set; }
        public int PopupCount {
            get {
                int count = 0;
                PopupBase[] popups = PopupContainer.GetComponentsInChildren<PopupBase>();
                foreach (PopupBase popup in popups) {
                    if (popup.gameObject.activeSelf) count += 1;
                }
                return count;
            }
        }
        #endregion

        #region Methods
        public static void PreloadPopups() {
            foreach (string path in PopupPath.Dictionary.Values) {
                GameObject res = Resources.Load<GameObject>(path);
                if (res) Instantiate(res);
                else CustomizedDebug.Log("Cannot find resources at " + path);
            }
        }
        #endregion
    }

    public abstract class PopupBase<T> : PopupBase where T : PopupBase<T> {
        [SerializeField] protected GameObject background;
        [SerializeField] protected RectTransform bgScaler;
        [SerializeField] protected bool isInstance = true;
        [SerializeField] protected bool onlyMe = true;
        protected bool loaded = false;
        protected Action callbackHide;

        #region Methods
        protected override void Awake() {
            base.Awake();
            if (isInstance) ServiceLocator.AddService(this as T);
        }

        protected override void OnEnable() {
            base.OnEnable();
            if (isInstance) {
                transform.SetParent(PopupContainer.transform);
                if (loaded) {
                    MakeSureOnShowWithOnlyMe();
                    Time.timeScale = 0;
                }
                else {
                    loaded = true;
                    Hide();
                }
            }
        }

        protected override void OnDisable() {
            base.OnDisable();
            if (PopupContainer == null) return;
            if (PopupCount == 0) Time.timeScale = 1;
            callbackHide?.Invoke();
            callbackHide = null;
        }

        public virtual void Show(Action onHide = null) {
            callbackHide = onHide;
            if (!gameObject.activeSelf) gameObject.SetActive(true);
        }

        protected void MakeSureOnShowWithOnlyMe() {
            if (PopupContainer == null) return;
            PopupBase []popups = PopupContainer.GetComponentsInChildren<PopupBase>();
            foreach (PopupBase popup in popups) {
                if (popup.gameObject != gameObject) {
                    if (onlyMe) popup.gameObject.SetActive(false);
                }
            }
        }

        public virtual void Hide() {
            gameObject.SetActive(false);
        }

        public void OnButtonHide() {
            Hide();
        }

        public static T GetInstance() {
            if (!ServiceLocator.IsServiceAvailable<T>()) CreateInstance();
            var instance = ServiceLocator.GetService<T>();
            return instance;
        }

        private static void CreateInstance() {
            string className = typeof(T).Name;
            string path = PopupPath.Dictionary[className];
            GameObject res = Resources.Load<GameObject>(path);
            if (res) Instantiate(res);
            else CustomizedDebug.Log("Can't find prefab: " + path);
        }

        protected virtual void OnDestroy() {
            if (isInstance) ServiceLocator.RemoveService(this as T);
        }

        public static bool IsShowing() {
            if (!ServiceLocator.IsServiceAvailable<T>()) return false;
            var instance = ServiceLocator.GetService<T>();
            return instance.isActiveAndEnabled;
        }
        #endregion
    }
}