using FastBoyFPS.Controller;
using FastBoyFPS.Manager;
using UnityEngine;

namespace FastBoyFPS {
    public class OverridableMonoBehaviour : MonoBehaviour {
        [SerializeField] private bool update = true;
        protected EventManager eventManager;
        protected DataManager dataManager;
        protected virtual void Awake() {
            if (!ServiceLocator.IsInitialized) ServiceLocator.Initialize();
            eventManager = ServiceLocator.GetService<EventManager>();
            dataManager = ServiceLocator.GetService<DataManager>();
        }
        protected virtual void OnEnable() {
            if (update) UpdateManager.AddItem(this);
        }

        protected virtual void OnDisable() {
            if (update) UpdateManager.RemoveSpecificItem(this);
        }

        public virtual void OnUpdate() { }
        public virtual void OnFixedUpdate() { }
        public virtual void OnLateUpdate() { }
    }
}
