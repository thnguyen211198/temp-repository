using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Manager;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public class LoadingController : MonoBehaviour {
        #region Methods
        private void Awake() {
            ServiceLocator.Initialize();
        }
        #endregion
    }
}