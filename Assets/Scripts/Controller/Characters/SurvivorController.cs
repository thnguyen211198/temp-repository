using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Component;
using FastBoyFPS.Entity;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public class SurvivorController : EntityController {
        [SerializeField] private Detector detector;
        private GameObject player;

        #region Methods
        private void Start() {
            entity = GetComponent<EntityBase>();
            navigator = gameObject.AddComponent<Navigator>();
            player = FirstPersonController.Player;
            detector.Target = player;
            IdleState = new IdleState(this);
            StunState = new StunState(this);
            WalkState = new WalkState(this);
            DeadState = new DeadState(this);
            AttackState = new AttackState(this);
            CurrentState = StunState;
            entity.OnDead.AddListener(() => Die());
            eventManager.AddListener(Manager.EventID.PlayerRescue, Rescue);
            StartCoroutine(FollowPlayer());
        }

        public override void OnUpdate() {
            if (CurrentState is WalkState) {
                navigator.Move(player.transform);
                if (detector.IsIntruded) {
                    CurrentState.Transition(AnimationKey.SpeedKey, 0);
                    navigator.Stop();
                }
            }
        }

        private void Rescue() {
            if (CurrentState is StunState) {
                if (detector.IsIntruded) {
                    CustomizedDebug.Log("Rescue " + name);
                    CurrentState.Transition(AnimationKey.SpeedKey, 0);
                    detector.DisableBound();
                    detector.ScaleRange(2);
                    eventManager.DispatcherListener(Manager.EventID.SurvivorRescued);
                }
            }
        }

        private IEnumerator FollowPlayer() {
            while (true) {
                yield return new WaitForSeconds(1.5f);
                if (CurrentState is IdleState) {
                    if (!detector.IsIntruded) {
                        CurrentState.Transition(AnimationKey.SpeedKey, navigator.MoveSpeed);
                        navigator.Move(player.transform);
                    }
                }
            }
        }

        private void Die() {
            CurrentState.Transition(AnimationKey.DieKey);
        }
        #endregion
    }
}