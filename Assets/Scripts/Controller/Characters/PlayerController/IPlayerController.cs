using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public interface IPlayerController {
        public void Config(FirstPersonController player);
        public void GetInput();
        public void LookAround();
        public void Move();
        public void Attack();
        public void Rescue();
    }
}