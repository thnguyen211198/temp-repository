using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Entity;
using FastBoyFPS.Manager;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public struct PlayerState {
        public static readonly int Idle = Animator.StringToHash("Base Layer.Idle");
    }

    public class FirstPersonController : OverridableMonoBehaviour {
        [Header("References: ")]
        [SerializeField] private Transform cam;
        [SerializeField] private CharacterController characterController;
        [SerializeField] private SphereCollider groundCheck;
        [SerializeField] private Hero human;
        [SerializeField] private ParticleSystem shootEffect;
        [SerializeField] private Animator animator;

        [Header("Player Settings: ")]
        [SerializeField] private float camSensitivity;
        [SerializeField] private float moveSpeed;
        [SerializeField] private LayerMask groundMask;

        private IPlayerController inputController;
        private Vector3 velocity;
        private bool isGrounded;

        #region Properties
        public float CamSensitivity => camSensitivity;
        public float MoveSpeed => moveSpeed;
        public CharacterController CharacterController => characterController;
        public static GameObject Player { get; private set; }
        #endregion

        #region Methods
        protected override void Awake() {
            base.Awake();
            Player = gameObject;
        }
        private void Start() {
            switch (PlatformManager.targetPlatform) {
                case TargetPlatform.Mobile:
                    inputController = cam.gameObject.AddComponent<PlayerMobileController>();
                    break;
                case TargetPlatform.Editor:
                    inputController = cam.gameObject.AddComponent<PlayerEditorController>();
                    break;
                case TargetPlatform.Undefined:
                    CustomizedDebug.Log("Undefined platform");
                    break;
            }
            //inputController = cam.gameObject.AddComponent<PlayerMobileController>();
            if (inputController != null) inputController.Config(this);
        }
        protected override void OnEnable() {
            base.OnEnable();
            eventManager.AddListener(EventID.PlayerShoot, Shoot);
            eventManager.AddListener(EventID.PlayerStopShoot, StopShoot);
            eventManager.AddListener(EventID.PlayerCut, Cut);
            eventManager.AddListener(EventID.PlayerThrow, Throw);
        }
        protected override void OnDisable() {
            base.OnDisable();
            eventManager.RemoveListener(EventID.PlayerShoot, Shoot);
            eventManager.RemoveListener(EventID.PlayerStopShoot, StopShoot);
            eventManager.RemoveListener(EventID.PlayerCut, Cut);
            eventManager.RemoveListener(EventID.PlayerThrow, Throw);
        }
        public override void OnUpdate() {
            isGrounded = Physics.CheckSphere(groundCheck.transform.position, groundCheck.radius, groundMask);
            if (isGrounded && velocity.y < 0) velocity.y = -1f;
            if (inputController != null) {
                inputController.GetInput();
                inputController.LookAround();
                inputController.Move();
                inputController.Attack();
                inputController.Rescue();
            }
            AttractedByGravity();
        }
        public void AttractedByGravity() {
            velocity += Physics.gravity * Time.deltaTime;
            characterController.Move(velocity * Time.deltaTime);
        }
        public void Shoot() {
            animator.SetBool(AnimationKey.Shoot2Key, true);
            if (!shootEffect.isPlaying) shootEffect.Play();
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            human.Shoot(ray);
        }
        public void StopShoot() {
            animator.SetBool(AnimationKey.Shoot2Key, false);
            shootEffect.Stop();
        }
        public void Cut() {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.fullPathHash == PlayerState.Idle) {
                int attackCode = Random.Range(0, 2) == 0 ? AnimationKey.Attack1Key : AnimationKey.Attack2Key;
                animator.SetTrigger(attackCode);
            }
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2);
            foreach (Collider hitCollider in hitColliders) {
                EntityBase entity = hitCollider.GetComponent<EntityBase>();
                if (entity != null && !entity.gameObject.Equals(gameObject)) {
                    entity.TakeDamage(100);
                }
            }
        }
        public void Throw() {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.fullPathHash == PlayerState.Idle) {
                animator.SetTrigger(AnimationKey.ThrowKey);
            }
        }
        #endregion
    }
}