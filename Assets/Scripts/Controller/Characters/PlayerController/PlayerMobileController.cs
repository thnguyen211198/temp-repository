using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Utils;
using FastBoyFPS.View;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public class PlayerMobileController : OverridableMonoBehaviour, IPlayerController {
        private int leftFingerId, rightFingerId;
        private float halfScreenWidth;
        private float camSensitivity;
        private Vector2 lookInput;
        private float cameraPitch;
        private Transform playerTransform;
        private VariableJoystick variableJoystick;
        private CharacterController characterController;
        private float speed;

        #region Methods
        protected override void Awake() {
            variableJoystick = FindObjectOfType<VariableJoystick>();
        }
        private void Start() {
            leftFingerId = -1;
            rightFingerId = -1;
            halfScreenWidth = Screen.width / 2;
        }
        public void Config(FirstPersonController player) {
            camSensitivity = player.CamSensitivity / 60;
            playerTransform = player.transform;
            characterController = player.CharacterController;
            speed = player.MoveSpeed;
        }
        public void GetInput() {
            for (int i = 0; i < Input.touchCount; i++) {
                Touch t = Input.GetTouch(i);
                switch (t.phase) {
                    case TouchPhase.Began:
                        if (t.position.x < halfScreenWidth && leftFingerId == -1) {
                            leftFingerId = t.fingerId;
                            CustomizedDebug.Log("Tracking left finger");
                        }
                        else if (t.position.x > halfScreenWidth && rightFingerId == -1) {
                            rightFingerId = t.fingerId;
                            CustomizedDebug.Log("Tracking right finger");
                        }
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        if (t.fingerId == leftFingerId) {
                            leftFingerId = -1;
                            CustomizedDebug.Log("Stopped tracking left finger");
                        }
                        else if (t.fingerId == rightFingerId) {
                            rightFingerId = -1;
                            CustomizedDebug.Log("Stopped tracking right finger");
                        }
                        break;
                    case TouchPhase.Moved:
                        if (t.fingerId == rightFingerId) lookInput = t.deltaPosition * camSensitivity * Time.deltaTime;
                        break;
                    case TouchPhase.Stationary:
                        if (t.fingerId == rightFingerId) lookInput = Vector2.zero;
                        break;
                }
            }
        }
        public void LookAround() {
            if (rightFingerId != -1) {
                cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -90f, 90f);
                transform.localRotation = Quaternion.Euler(cameraPitch, 0, 0);
                playerTransform.Rotate(transform.up, lookInput.x);
            }
        }
        public void Move() {
            Vector3 move = transform.right * variableJoystick.Horizontal + transform.forward * variableJoystick.Vertical;
            characterController.Move(move * speed * Time.deltaTime);
        }
        public void Attack() {

        }
        public void Rescue() {

        }
        #endregion
    }

}
