using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Manager;
using FastBoyFPS.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FastBoyFPS.Controller {
    public class PlayerEditorController : OverridableMonoBehaviour, IPlayerController {
        private Transform playerTransform;
        //Rotation: 
        private float camSensitivity;
        private float mouseX;
        private float mouseY;
        private float xRotation = 0f;
        //"Movement:
        private CharacterController characterController;
        private Vector3 move;
        private float speed;

        #region Methods
        private void Start() {
            Cursor.lockState = CursorLockMode.Locked;
        }
        public void Config(FirstPersonController player) {
            camSensitivity = player.CamSensitivity;
            playerTransform = player.transform;
            characterController = player.CharacterController;
            speed = player.MoveSpeed;
        }

        public void GetInput() {
            mouseX = Input.GetAxis("Mouse X") * camSensitivity * Time.deltaTime;
            mouseY = Input.GetAxis("Mouse Y") * camSensitivity * Time.deltaTime;
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            move = transform.right * x + transform.forward * z;
            //CustomizedDebug.Log("Move direction: " + move.ToString());
            if (Input.GetKeyDown(KeyCode.P)) {
                SceneManager.LoadScene(0);
                Cursor.lockState = CursorLockMode.None;
            }
        }

        public void LookAround() {
            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerTransform.Rotate(Vector3.up * mouseX);
        }

        public void Move() {
            characterController.Move(move * speed * Time.deltaTime);
        }

        public void Attack() {
            if (Input.GetMouseButtonUp(0)){
                eventManager.DispatcherListener(EventID.PlayerStopShoot);
            }
            else if (Input.GetMouseButton(0)) {
                eventManager.DispatcherListener(EventID.PlayerShoot);
            }
            else if (Input.GetKeyDown(KeyCode.Space)) {
                eventManager.DispatcherListener(EventID.PlayerCut);
            }
            else if (Input.GetKeyDown(KeyCode.B)) {
                eventManager.DispatcherListener(EventID.PlayerThrow);
            }
        }
        public void Rescue() {
            if (Input.GetKeyDown(KeyCode.N)) {
                eventManager.DispatcherListener(EventID.PlayerRescue);
            }
        }
        #endregion
    }

}