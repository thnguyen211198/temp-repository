using System.Collections;
using FastBoyFPS.Component;
using FastBoyFPS.Entity;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public class ZombieController : EntityController {
        [SerializeField] private EntityBase target;        
        protected (float, float) stunDelay = (2.5f, 4f);
        private Coroutine stunCoroutine;
        private int currentAttackType;
        private Cuttable weapon;

        #region Methods
        protected override void Awake() {
            base.Awake();
            entity = GetComponent<EntityBase>();
            navigator = gameObject.AddComponent<Navigator>();
        }
        private void Start() {
            target = FirstPersonController.Player.GetComponent<Hero>();
            weapon = (entity as Zombie).Weapon;
            InitState();            
            entity.OnShotBody.AddListener(() => ApplyStun(AnimationKey.GetHitBodyKey));
            entity.OnShotHead.AddListener(() => ApplyStun(AnimationKey.GetHitHeadKey));
            entity.OnDead.AddListener(() => Die());
            StartCoroutine(UpdateAttackType());
        }
        public void InitState() {
            IdleState = new IdleState(this);
            StunState = new StunState(this);
            WalkState = new WalkState(this);
            DeadState = new DeadState(this);
            AttackState = new AttackState(this);
            CurrentState = IdleState;
        }
        public override void OnUpdate() {
            if (CurrentState is IdleState) {
                CurrentState.Transition(AnimationKey.SpeedKey, navigator.MoveSpeed);
                navigator.Move(target.transform);
            }
            else if (CurrentState is WalkState) {
                if (target) {
                    navigator.Move(target.transform);
                    if (navigator.IsReach) {
                        transform.LookAt(target.transform);
                        CurrentState.Transition(currentAttackType);
                        weapon.Vimtim = target;
                    }
                }
            }
            else if (CurrentState is AttackState) {
                navigator.Move(target.transform);
                if (!navigator.IsReach) CurrentState.Transition(AnimationKey.SpeedKey, navigator.MoveSpeed);
                else navigator.Stop();
            }
            else {
                navigator.Stop();
            }
            //CustomizedDebug.Log(navigator.RemainingDistance.ToString());
        }
        public void ApplyStun(int actionCode) {
            if (stunCoroutine == null) stunCoroutine = StartCoroutine(Stun(actionCode));
        }
        private IEnumerator Stun(int actionCode) {
            CurrentState.Transition(actionCode);
            yield return new WaitForSeconds(1f);
            CurrentState.Transition(AnimationKey.SpeedKey, 0f);
            float delay = Random.Range(stunDelay.Item1, stunDelay.Item2);
            yield return new WaitForSeconds(delay);
            stunCoroutine = null;
        }
        private void Die() {
            CurrentState.Transition(AnimationKey.DieKey);
            eventManager.DispatcherListener(Manager.EventID.ZombieKilled);
        }
        private IEnumerator UpdateAttackType() {
            while (true) {
                int rdAttack = Random.Range(0, 2);
                if (rdAttack == 1) currentAttackType = AnimationKey.Attack1Key;
                else currentAttackType = AnimationKey.Attack2Key;
                yield return new WaitForSeconds(2f);
            }
        }
        #endregion
    }
}