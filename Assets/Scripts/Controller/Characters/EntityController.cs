using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Component;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public abstract class EntityController : OverridableMonoBehaviour {
        protected Navigator navigator;
        protected EntityBase entity;

        #region Properties
        public EntityBase Entity => entity;
        public IEntityState CurrentState { get; set; }
        public Navigator Navigator => navigator;
        public IdleState IdleState { get; protected set; }
        public StunState StunState { get; protected set; }
        public WalkState WalkState { get; protected set; }
        public DeadState DeadState { get; protected set; }
        public AttackState AttackState { get; protected set; }
        #endregion
    }
}