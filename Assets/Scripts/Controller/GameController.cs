using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Data;
using FastBoyFPS.Entity;
using FastBoyFPS.Utils;
using FastBoyFPS.View;
using UnityEngine;

namespace FastBoyFPS.Controller {
    public class GameController : OverridableMonoBehaviour {
        [SerializeField] private Transform []survivorSpawnPoints;
        [SerializeField] private Survivor survivor;
        [SerializeField] private Zombie zombie;
        [SerializeField] private ZombieWave[] waves;
        private int currentWave = 0;
        private float lastWave = 0f;
        private GameObject player;
        private GameObject survivorContainer;
        private GameObject zombieContainer;
        private float cachedTime;

        #region Properties
        public Transform []SurvivorSpawnPoints => survivorSpawnPoints;
        #endregion

        #region Methods
        private void OnDrawGizmos() {
            foreach(Transform p in survivorSpawnPoints) {
                Gizmos.DrawSphere(p.position, 5f);
            }
        }
        private void Start() {
            survivorContainer = new GameObject("SurvivorContainer");
            zombieContainer = new GameObject("ZombieContainer");
            foreach (Transform p in survivorSpawnPoints) {
                GameObject s = Instantiate(survivor.gameObject, p.position, Quaternion.identity);
                s.transform.SetParent(survivorContainer.transform);
            }
            player = FirstPersonController.Player;
            lastWave = Time.time;
            cachedTime = Time.time;
            StartCoroutine(SpawnZombie());
            player.GetComponent<EntityBase>().OnDead.AddListener(GameOver);
        }
        protected override void OnEnable() {
            base.OnEnable();
        }
        protected override void OnDisable() {
            base.OnDisable();
        }

        private IEnumerator SpawnZombie() {
            while (true) {
                if (Time.time > lastWave + waves[currentWave].Duration) MoveNextWave();
                if (waves[currentWave].Interval > 0.5f) {
                    yield return new WaitForSeconds(waves[currentWave].Interval);
                     if (player != null) {
                        int n = waves[currentWave].Unit;
                        for (int i = 0; i < n; i++) {
                            Vector3 zombieSpawnPoint = GetRandomPosition();
                            //CustomizedDebug.Log("Spawned Zombie at " + zombieSpawnPoint);
                            GameObject spawnedZombie = Instantiate(zombie.gameObject, zombieSpawnPoint, Quaternion.identity);
                            spawnedZombie.transform.SetParent(zombieContainer.transform);
                        }
                    }
                }
                else {
                    yield return new WaitForSeconds(1f);
                    CustomizedDebug.LogWarning("Interval is too low");
                }
            }
        }

        private void MoveNextWave() {
            lastWave = Time.time;
            if (currentWave < waves.Length - 1) currentWave++;
            CustomizedDebug.Log("Wave: " + (currentWave + 1));
        }

        private Vector3 GetRandomPosition() {
            float offset = Random.Range(0, 2) == 0 ? Random.Range(-50, 20) : Random.Range(20, 50);
            float x = player.transform.position.x + offset;
            offset = Random.Range(0, 2) == 0 ? Random.Range(-50, 20) : Random.Range(20, 50);
            float z = player.transform.position.z + offset;
            return new Vector3(x, 0, z);
        }

        private void GameOver() {
            dataManager.SurvivalTime = Time.time - cachedTime;
            PopupGameOver.GetInstance().Show();
            Cursor.lockState = CursorLockMode.None;
        }
        #endregion
    }
}