using System.Collections.Generic;
using UnityEngine.Events;

namespace FastBoyFPS.Manager {
    public enum EventID {
        PlayerShoot,
        PlayerStopShoot,
        PlayerCut,
        PlayerThrow,
        PlayerRescue,
        PlayerDie,
        SurvivorRescued,
        ZombieKilled,
        PlayerMeetSurvivor,
        PlayerLeaveSurvivor,
    }
    public class EventManager {
        private readonly Dictionary<EventID, UnityEvent> events = new Dictionary<EventID, UnityEvent>();
        #region Methods
        public void AddListener(EventID id, UnityAction param) {
            if (!events.ContainsKey(id)) {
                UnityEvent newEvent = new UnityEvent();
                events.Add(id, newEvent);
            }
            events[id].AddListener(param);
        }

        public void RemoveListener(EventID id, UnityAction param) {
            if (events.ContainsKey(id)) events[id].RemoveListener(param);
        }

        public void DispatcherListener(EventID id) {
            if (events.ContainsKey(id)) events[id].Invoke();
        }
        protected void Destroy() {
            events.Clear();
        }
        #endregion Methodss
    }
}