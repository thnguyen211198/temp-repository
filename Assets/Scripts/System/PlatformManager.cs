using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Manager {
    public enum TargetPlatform { Mobile, Editor, Undefined }
    public class PlatformManager {
        public static TargetPlatform targetPlatform = TargetPlatform.Undefined;
        #region Methods
        public void Init() {
            if (
                Application.platform == RuntimePlatform.Android ||
                Application.platform == RuntimePlatform.IPhonePlayer
            ) {
                targetPlatform = TargetPlatform.Mobile;
            }
            else if (
                Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.LinuxEditor ||
                Application.platform == RuntimePlatform.OSXEditor
            ) {
                targetPlatform = TargetPlatform.Editor;
            }
        }
        #endregion
    }
}
