using System;
using UnityEngine;


namespace FastBoyFPS.Manager {
    public struct PlayerPrefKeys {
        public static string Session = "Session";
        public static string Coin = "Coin";
    }

    public class DataManager {
        #region Properties
        public int Session { get; set; }
        public int Coin { get; set; }
        public int RescueAmount { get; set; }
        public int KillAmount { get; set; }
        public float SurvivalTime { get; set; }
        #endregion

        #region Methods
        public void Init() {
            LoadData();
            Session++;
        }

        public void SetDataNewGame() {
            RescueAmount = 0;
            KillAmount = 0;
            SurvivalTime = 0;
        }

        protected virtual void LoadData() {
            Session = PlayerPrefs.GetInt(PlayerPrefKeys.Session, 0);
            Coin = PlayerPrefs.GetInt(PlayerPrefKeys.Coin, 1000);
        }

        protected virtual void SaveData() {
            PlayerPrefs.SetInt(PlayerPrefKeys.Session, Session);
            PlayerPrefs.SetInt(PlayerPrefKeys.Coin, Coin);
        }

        public void Save() {
            SaveData();
            PlayerPrefs.Save();
        }
        #endregion
    }
}