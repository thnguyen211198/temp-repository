using System;
using FastBoyFPS.Utils;

namespace FastBoyFPS.Controller {
    public class UpdateManager : SingletonPattern<UpdateManager> {
        protected override bool DestroyOnLoad => false;
        private static event Action OnUpdateEvent;
        private static event Action OnFixedUpdateEvent;
        private static event Action OnLateUpdateEvent;

        private static readonly Type overridableMonoBehaviourType = typeof(OverridableMonoBehaviour);

        public static void SubscribeToUpdate(Action callback) {
            if (Instance == null) return;
            OnUpdateEvent += callback;
        }

        public static void SubscribeToFixedUpdate(Action callback) {
            if (Instance == null) return;
            OnFixedUpdateEvent += callback;
        }

        public static void SubscribeToLateUpdate(Action callback) {
            if (Instance == null) return;
            OnLateUpdateEvent += callback;
        }

        public static void UnsubscribeFromUpdate(Action callback) {
            OnUpdateEvent -= callback;
        }

        public static void UnsubscribeFromFixedUpdate(Action callback) {
            OnFixedUpdateEvent -= callback;
        }

        public static void UnsubscribeFromLateUpdate(Action callback) {
            OnLateUpdateEvent -= callback;
        }

        public static void AddItem(OverridableMonoBehaviour behaviour) {
            if (behaviour == null) throw new NullReferenceException("The behaviour you've tried to add is null!");
            if (isShuttingDown) return;
            AddItemToArray(behaviour);
        }

        public static void RemoveSpecificItem(OverridableMonoBehaviour behaviour) {
            if (behaviour == null) throw new NullReferenceException("The behaviour you've tried to remove is null!");
            if (isShuttingDown) return;
            if (Instance != null) RemoveSpecificItemFromArray(behaviour);
        }

        public static void RemoveSpecificItemAndDestroyComponent(OverridableMonoBehaviour behaviour) {
            if (behaviour == null) throw new NullReferenceException("The behaviour you've tried to remove is null!");
            if (isShuttingDown) return;
            if (Instance != null) RemoveSpecificItemFromArray(behaviour);
            Destroy(behaviour);
        }

        public static void RemoveSpecificItemAndDestroyGameObject(OverridableMonoBehaviour behaviour) {
            if (behaviour == null) throw new NullReferenceException("The behaviour you've tried to remove is null!");
            if (isShuttingDown) return;
            if (Instance != null) RemoveSpecificItemFromArray(behaviour);
            Destroy(behaviour.gameObject);
        }

        private static void AddItemToArray(OverridableMonoBehaviour behaviour) {
            Type behaviourType = behaviour.GetType();
            if (behaviourType.GetMethod("OnUpdate").DeclaringType != overridableMonoBehaviourType) {
                SubscribeToUpdate(behaviour.OnUpdate);
            }
            if (behaviourType.GetMethod("OnFixedUpdate").DeclaringType != overridableMonoBehaviourType) {
                SubscribeToFixedUpdate(behaviour.OnFixedUpdate);
            }
            if (behaviourType.GetMethod("OnLateUpdate").DeclaringType != overridableMonoBehaviourType) {
                SubscribeToLateUpdate(behaviour.OnLateUpdate);
            }
        }

        private static void RemoveSpecificItemFromArray(OverridableMonoBehaviour behaviour) {
            UnsubscribeFromUpdate(behaviour.OnUpdate);
            UnsubscribeFromFixedUpdate(behaviour.OnFixedUpdate);
            UnsubscribeFromLateUpdate(behaviour.OnLateUpdate);
        }

        private void Update() {
            if (OnUpdateEvent != null) OnUpdateEvent.Invoke();
        }

        private void FixedUpdate() {
            if (OnFixedUpdateEvent != null) OnFixedUpdateEvent.Invoke();
        }

        private void LateUpdate() {
            if (OnLateUpdateEvent != null) OnLateUpdateEvent.Invoke();
        }
    }
}