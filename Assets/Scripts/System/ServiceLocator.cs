using System;
using System.Collections.Generic;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Manager {
    public class ServiceLocator {
        public static bool IsInitialized = false;
        private static Dictionary<object, object> services = new Dictionary<object, object>();
        
        #region Methods
        public static void Initialize() {
            if (IsInitialized) CustomizedDebug.LogWarning("Service locator already initialize.");
            else {
                AddService(new PlatformManager()).Init();
                AddService(new EventManager());
                AddService(new DataManager()).Init();
                //AddService(new BoosterManager()).Init();
                IsInitialized = true;
            }
        }

        public static T AddService<T>(T service) {
            if (services.ContainsKey(typeof(T))) CustomizedDebug.LogWarning("Service already added.");
            else services.Add(typeof(T), service);
            return service;
        }

        public static T GetService<T>() {
            try {
                return (T)services[typeof(T)];
            }
            catch (Exception ex) {
                throw new NullReferenceException($"Service {typeof(T)} not available {ex}");
            }
        }

        public static void RemoveService<T>(T service) {
            if (services.ContainsKey(typeof(T)) && services[typeof(T)].Equals(service)) {
                services.Remove(typeof(T));
            }
        }

        public static bool IsServiceAvailable<T>() {
            return services.ContainsKey(typeof(T));
        }
        #endregion
    }
}