using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Component;
using FastBoyFPS.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace FastBoyFPS.Entity {
    public struct AnimationKey {
        public static readonly int SpeedKey = Animator.StringToHash("Move_Speed");
        public static readonly int GetHitBodyKey = Animator.StringToHash("Get_Hit_Body");
        public static readonly int GetHitHeadKey = Animator.StringToHash("Get_Hit_Head");
        public static readonly int Shoot1Key = Animator.StringToHash("Shoot_1");
        public static readonly int Shoot2Key = Animator.StringToHash("Shoot_2");
        public static readonly int Attack1Key = Animator.StringToHash("Attack_1");
        public static readonly int Attack2Key = Animator.StringToHash("Attack_2");
        public static readonly int ThrowKey = Animator.StringToHash("Throw");
        public static readonly int DieKey = Animator.StringToHash("Die");
    }

    public class EntityBase : MonoBehaviour, IEntity {
        [Header("References: ")]
        [SerializeField] protected Collider head;
        [SerializeField] protected Collider body;
        [SerializeField] protected Animator animator;
        [SerializeField] protected ParticleSystem bloodParticle;
        [SerializeField] protected Health health;

        #region Properties
        public Animator Animator => animator;
        public bool IsAlive => health.HP > 0;
        public float AttackSpeed { get; private set; } = 1f;
        #endregion

        #region Events
        [HideInInspector] public UnityEvent OnShotHead = new UnityEvent();
        [HideInInspector] public UnityEvent OnShotBody = new UnityEvent();
        [HideInInspector] public UnityEvent OnDead = new UnityEvent();
        #endregion

        #region Methods
        protected virtual void Start() {
            OnDead.AddListener(Blood);
            if (head == null || body == null) CustomizedDebug.Log("Object " + name + " has not been assigned collider");
        }
        public void TakeDamage(Collider colliderHit, int damage) {
            if (IsAlive) {
                if (colliderHit.Equals(head)) {
                    CustomizedDebug.Log("Headshot");
                    health.GetHit(damage * 2);
                    OnShotHead.Invoke();
                }
                else if (colliderHit.Equals(body)) {
                    CustomizedDebug.Log("Bodyshot");
                    health.GetHit(damage);
                    OnShotBody.Invoke();
                }
                else CustomizedDebug.Log("Shoot Error");
            }
        }
        public void TakeDamage(int damage) {
            health.GetHit(damage);
        }

        protected void Blood() {
            if (bloodParticle != null) bloodParticle.Play();
            StartCoroutine(DestoyAfter(2f));
        }

        private IEnumerator DestoyAfter(float seconds) {
            yield return new WaitForSeconds(seconds);
            Destroy(gameObject);
        }
        #endregion
    }
}