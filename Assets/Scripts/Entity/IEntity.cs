using UnityEngine;

namespace FastBoyFPS.Entity {
    public interface IEntity {
        public void TakeDamage(Collider colliderHit, int damage);
    }
}