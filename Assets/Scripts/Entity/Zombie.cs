using FastBoyFPS.Component;
using FastBoyFPS.Controller;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Entity {
    public class Zombie : EntityBase {
        [Header("Figures: ")]
        [SerializeField] private GameObject[] figures;

        #region Properties
        public Cuttable Weapon { get; set; }
        #endregion

        #region Methods
        private void Awake() {
            RandomFigure();
        }
        protected override void Start() {
            base.Start();
        }
        private void RandomFigure() {
            GameObject selected = figures[Random.Range(0, figures.Length)];
            foreach (GameObject f in figures) {
                if (!f.Equals(selected)) Destroy(f);
            }
            selected.SetActive(true);
            animator = selected.GetComponent<Animator>();
            Weapon = selected.GetComponent<Cuttable>();
        }
        #endregion
    }
}