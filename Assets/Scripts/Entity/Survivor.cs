using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FastBoyFPS.Entity {
    public class Survivor : EntityBase {
        [Header("Figures: ")]
        [SerializeField] private GameObject []figures;
        #region Methods
        protected override void Start() {
            base.Start();
            RandomFigure();
        }
        private void RandomFigure() {
            GameObject selected = figures[Random.Range(0, figures.Length)];
            IEnumerable<GameObject> filter = figures.Where(f => !f.Equals(selected));
            foreach (GameObject figure in filter) Destroy(figure);
            selected.SetActive(true);
            animator = selected.GetComponent<Animator>();
        }

        #endregion
    }
}