using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Component;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Entity {
    public class Hero : EntityBase {
        [SerializeField] private Shootable shootable;
        [SerializeField] private Cuttable cuttable;
        [SerializeField] private Throwable throwable;
        private IEntity target;

        #region Methods
        protected override void Start() {
            base.Start();
            if (shootable == null) shootable = gameObject.AddComponent<Shootable>();
            if (cuttable == null) cuttable = gameObject.AddComponent<Cuttable>();
            if (throwable == null) throwable = gameObject.AddComponent<Throwable>();
        }

        public void Attack() {
            Ray ray = new Ray(transform.position, transform.forward);
            Shoot(ray);
        }
        public void Shoot(Ray ray) {
            if (shootable.CanShoot) {
                shootable.Shoot(ray);
                //CustomizedDebug.Log("Player shoot");
            }
        }
        #endregion
    }
}