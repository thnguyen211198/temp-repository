using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Data {
    [CreateAssetMenu(fileName = "Data", menuName = "Configuration/PlayerData", order = 2)]
    public class PlayerData : ScriptableObject {
        public int HP;
        public float MoveSpeed;
        public int GunDamage;
        public int KnifeDamage;
        public int GrenadeDamage;
    }
}