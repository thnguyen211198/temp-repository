using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Data {
    [CreateAssetMenu(fileName ="Data", menuName = "Configuration/ZombieWave", order = 1)]
    public class ZombieWave : ScriptableObject {
        public int Damage = 100;
        public int Unit = 10;
        public float Interval = 1f;
        public float Duration = 20f;
    }
}