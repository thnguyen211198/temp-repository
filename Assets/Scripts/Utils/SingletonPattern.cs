using UnityEngine;

namespace FastBoyFPS.Utils {
    public abstract class SingletonPattern<T> : MonoBehaviour where T : MonoBehaviour {
        protected static bool isShuttingDown = false;
        private static T instance;
        private static object objectLock;
        private static readonly System.Type instanceType = typeof(T);

        #region Properties
        protected abstract bool DestroyOnLoad { get; }

        public static T Instance {
            get {
                if (isShuttingDown) {
                    CustomizedDebug.LogWarning("Tried to access " + instanceType.Name + " while the application is going to quit! This is not allowed.");
                    return null;
                }

                if (objectLock == null) objectLock = new object();
                lock (objectLock) {
                    if (instance != null) return instance;
                    instance = (T)FindObjectOfType(instanceType);
                    if (instance != null) return instance;
                    instance = (T)new GameObject(instanceType.Name).AddComponent(instanceType);
                    SingletonPattern<T> singleton = instance as SingletonPattern<T>;
                    if (singleton != null && !singleton.DestroyOnLoad) DontDestroyOnLoad(instance);
                    return instance;
                }
            }
        }
        #endregion

        #region Events
        private void OnApplicationQuit() {
            isShuttingDown = true;
        }
        private void OnDestroy() {
            isShuttingDown = true;
        }
        #endregion
    }
}