using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Utils {
    public class ChildControlTool : MonoBehaviour {
        [SerializeField] private string childName;
        [SerializeField] private string groupName;
        [SerializeField] private int groupSize;

        #region Methods
        public void Reverse() {
            for (int i = 0; i < transform.childCount; i++) {
                Transform t = transform.GetChild(0);
                t.SetSiblingIndex(transform.childCount - 1 - i);
            }
        }
        public void SetName() {
            for (int i = 0; i < transform.childCount; i++) {
                Transform t = transform.GetChild(i);
                if (i == 0) t.name = childName + "0" + (i + 1);
                else t.name = childName + (i + 1);
            }
        }
        public void Group() {
            if (groupSize > 1) {
                List<Transform> children = new List<Transform>();
                for (int i = 0; i < transform.childCount; i++) {
                    Transform t = transform.GetChild(i);
                    children.Add(t);
                }
                int count = 0;
                GameObject group = null;
                foreach (Transform child in children) {
                    if (count % groupSize == 0) {
                        group = new GameObject(groupName);
                        group.transform.parent = transform;
                    }
                    child.parent = group.transform;
                    count++;
                }
            }
        }

        public void AddColliderInChildren() {
            for (int i = 0; i < transform.childCount; i++) {
                Transform t = transform.GetChild(i);
                if (t.gameObject.GetComponent<BoxCollider>() == null) t.gameObject.AddComponent<BoxCollider>();
            }
        }

        public void ModifyCollider() {
            for (int i = 0; i < transform.childCount; i++) {
                Transform t = transform.GetChild(i);
                BoxCollider collider = t.gameObject.GetComponent<BoxCollider>();
                if (collider != null) {
                    collider.size = new Vector3(collider.size.x, collider.size.y + 1, collider.size.z);
                    collider.center = new Vector3(collider.center.x, collider.size.y / 2, collider.center.z);
                }
            }
        }
        #endregion
    }
}