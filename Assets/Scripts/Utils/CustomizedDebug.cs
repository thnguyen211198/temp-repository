using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Manager;
using UnityEngine;

namespace FastBoyFPS.Utils {
    public class CustomizedDebug : MonoBehaviour {
        #region Methods
        public static void Log(string message) {
            if (PlatformManager.targetPlatform == TargetPlatform.Editor) Debug.Log(message);
        }

        public static void LogWarning(string message) {
            if (PlatformManager.targetPlatform == TargetPlatform.Editor) Debug.LogWarning(message);
        }

        public static void LogError(string message) {
            if (PlatformManager.targetPlatform == TargetPlatform.Editor) Debug.LogError(message);
        }
        #endregion
    }
}