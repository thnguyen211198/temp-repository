using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace FastBoyFPS.Component {
    public class Navigator : MonoBehaviour {
        NavMeshAgent agent;

        #region Properties
        public float MoveSpeed => agent.speed;
        public float RemainingDistance => agent.remainingDistance;
        public bool IsReach => !agent.pathPending && (agent.remainingDistance <= agent.stoppingDistance);
        #endregion

        #region Methods
        private void Start() {
            agent = GetComponent<NavMeshAgent>();
        }
        public void Move(Transform target) {
            agent.isStopped = false;
            agent.SetDestination(target.position);
        }
        public void Stop() {
            agent.isStopped = true;
        }
        #endregion
    }
}