using FastBoyFPS.Controller;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class WalkState : IEntityState {
        EntityController entityController;
        Animator animator;

        #region Constructor
        public WalkState(EntityController e) {
            entityController = e;
            animator = e.Entity.Animator;
        }
        #endregion

        #region Methods
        public void Transition(int actionCode, float? value = null) {
            if (actionCode == AnimationKey.SpeedKey) {
                animator.SetFloat(AnimationKey.SpeedKey, value.Value);
                if (value <= 0) entityController.CurrentState = entityController.IdleState;
            }
            else if (actionCode == AnimationKey.GetHitBodyKey || actionCode == AnimationKey.GetHitHeadKey) {
                animator.SetFloat(AnimationKey.SpeedKey, 0);
                animator.SetBool(actionCode, true);
                entityController.CurrentState = entityController.StunState;
            }
            else if (actionCode == AnimationKey.DieKey) {
                animator.SetTrigger(actionCode);
                entityController.CurrentState = entityController.DeadState;
            }
            else if (actionCode == AnimationKey.Attack1Key || actionCode == AnimationKey.Attack2Key) {
                animator.SetBool(actionCode, true);
                entityController.CurrentState = entityController.AttackState;
            }
        }
        #endregion
    }
}