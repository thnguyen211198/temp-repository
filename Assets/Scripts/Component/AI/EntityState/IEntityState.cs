
namespace FastBoyFPS.Component {
    public interface IEntityState {
        public void Transition(int actionCode, float? value = null);
    }
}