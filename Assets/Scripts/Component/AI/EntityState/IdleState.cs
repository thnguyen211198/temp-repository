using FastBoyFPS.Controller;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class IdleState : IEntityState {
        EntityController entityController;
        Animator animator;

        #region Constructor
        public IdleState(EntityController e) {
            entityController = e;
            animator = e.Entity.Animator;
        }
        #endregion

        #region Methods
        public void Transition(int actionCode, float? value = null) {
            if (actionCode == AnimationKey.SpeedKey) {
                if (value > 0) {
                    animator.SetFloat(AnimationKey.SpeedKey, entityController.Navigator.MoveSpeed);
                    entityController.CurrentState = entityController.WalkState;
                }
            }
            else if (actionCode == AnimationKey.GetHitBodyKey || actionCode == AnimationKey.GetHitHeadKey) {
                animator.SetBool(actionCode, true);
                entityController.CurrentState = entityController.StunState;
            }
            else if (actionCode == AnimationKey.DieKey) {
                animator.SetTrigger(actionCode);
                entityController.CurrentState = entityController.DeadState;
            }
            else if (actionCode == AnimationKey.Attack1Key || actionCode == AnimationKey.Attack2Key) {
                animator.SetBool(actionCode, true);
                entityController.CurrentState = entityController.AttackState;
            }
        }
        #endregion
    }
}