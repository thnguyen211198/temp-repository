using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Controller;
using FastBoyFPS.Entity;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class StunState : IEntityState {
        EntityController entityController;
        Animator animator;

        #region Constructor
        public StunState(EntityController e) {
            entityController = e;
            animator = e.Entity.Animator;
        }
        #endregion

        #region Methods
        public void Transition(int actionCode, float? value = null) {
            if (actionCode == AnimationKey.SpeedKey) {
                animator.SetBool(AnimationKey.GetHitBodyKey, false);
                animator.SetBool(AnimationKey.GetHitHeadKey, false);
                if (value > 0) {
                    animator.SetFloat(AnimationKey.SpeedKey, entityController.Navigator.MoveSpeed);
                    entityController.CurrentState = entityController.WalkState;
                }
                else {
                    animator.SetFloat(AnimationKey.SpeedKey, 0);
                    entityController.CurrentState = entityController.IdleState;
                }
            }
            else if (actionCode == AnimationKey.DieKey) {
                animator.SetTrigger(AnimationKey.DieKey);
                entityController.CurrentState = entityController.DeadState;
            }
        }
        #endregion
    }
}