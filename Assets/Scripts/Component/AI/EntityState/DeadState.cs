using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Controller;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class DeadState : IEntityState {
        EntityController entityController;
        Animator animator;

        #region Constructor
        public DeadState(EntityController e) {
            entityController = e;
            animator = e.Entity.Animator;
        }
        #endregion

        #region Methods
        public void Transition(int actionCode, float? value = null) {

        }
        #endregion
    }
}