using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Controller;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class AttackState : IEntityState {
        EntityController entityController;
        Animator animator;

        #region Constructor
        public AttackState(EntityController e) {
            entityController = e;
            animator = e.Entity.Animator;
        }
        #endregion

        #region Methods
        public void Transition(int actionCode, float? value = null) {
            if (actionCode == AnimationKey.SpeedKey) {
                animator.SetBool(AnimationKey.Attack1Key, false);
                animator.SetBool(AnimationKey.Attack2Key, false);
                if (value > 0) {
                    animator.SetFloat(AnimationKey.SpeedKey, entityController.Navigator.MoveSpeed);
                    entityController.CurrentState = entityController.WalkState;
                }
                else {
                    animator.SetFloat(AnimationKey.SpeedKey, 0);
                    entityController.CurrentState = entityController.IdleState;
                }
            }
            else if (actionCode == AnimationKey.DieKey) {
                animator.SetTrigger(AnimationKey.DieKey);
                entityController.CurrentState = entityController.DeadState;
            }
            else if (actionCode == AnimationKey.Attack1Key) {
                animator.SetBool(AnimationKey.Attack1Key, true);
                animator.SetBool(AnimationKey.Attack2Key, false);
            }
            else if (actionCode == AnimationKey.Attack2Key) {
                animator.SetBool(AnimationKey.Attack1Key, false);
                animator.SetBool(AnimationKey.Attack2Key, true);
            }
        }
        #endregion
    }
}
