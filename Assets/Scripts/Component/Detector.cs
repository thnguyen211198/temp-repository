using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Detector : MonoBehaviour {
        private SpriteRenderer bound;
        private SphereCollider range;
        #region Properties
        public GameObject Target { get; set; }
        public bool IsIntruded { get; private set; } = false;
        #endregion

        #region Methods
        private void Start() {
            bound = GetComponent<SpriteRenderer>();
            range = GetComponent<SphereCollider>();
        }
        public void DisableBound() {
            Destroy(bound);
        }
        public void ScaleRange(float ratio) {
            range.radius *= ratio;
        }
        private void OnTriggerEnter(Collider other) {
            CustomizedDebug.Log(other.name);
            if (other.gameObject.Equals(Target)) {
                IsIntruded = true;
                if (bound) bound.color = new Color(0, 1, 0, 0.4f);
            }
        }
        private void OnTriggerExit(Collider other) {
            if (other.gameObject.Equals(Target)) {
                IsIntruded = false;
                if (bound) bound.color = new Color(1, 0, 0, 0.4f);
            }
        }
        #endregion
    }
}