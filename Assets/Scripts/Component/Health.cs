using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Entity;
using UnityEngine;
using UnityEngine.UI;

namespace FastBoyFPS.Component {
    public class Health : MonoBehaviour {
        [SerializeField] private EntityBase entity;
        [SerializeField] private int maxHP = 1000;
        [SerializeField] private Image bar;

        #region Properties
        public int HP { get; private set; }
        #endregion

        #region Methods
        private void Start() {
            HP = maxHP;
        }
        public void GetHit(int damage) {
            HP -= damage;
            if (HP <= 0) {
                HP = 0;
                entity.OnDead.Invoke();
            }
            if (bar) bar.fillAmount = (float)HP / maxHP;
        }
        #endregion
    }
}
