using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Controller;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class GuideLine : OverridableMonoBehaviour {
        [SerializeField] GameController gameController;
        private List<Transform> destinations;
        private Transform nearstSurvivor;
        private LineRenderer line;
        private GameObject player;

        #region Methods
        private void Start() {
            destinations = new List<Transform>(gameController.SurvivorSpawnPoints);
            player = FirstPersonController.Player;
            line = gameObject.AddComponent<LineRenderer>();
            line.positionCount = 2;
            StartCoroutine(UpdateNearst());
        }
        protected override void OnEnable() {
            base.OnEnable();
            eventManager.AddListener(Manager.EventID.SurvivorRescued, PopNearst);
        }
        protected override void OnDisable() {
            base.OnDisable();
            eventManager.RemoveListener(Manager.EventID.SurvivorRescued, PopNearst);
        }
        public override void OnUpdate() {
            Vector3 fromPlayer = player.transform.position;
            fromPlayer.y += 10;
            line.SetPosition(1, fromPlayer);
        }

        private void PopNearst() {
            destinations.Remove(nearstSurvivor);
            SetNearstSurvivor();
        }
        private IEnumerator UpdateNearst() {
            SetNearstSurvivor();
            yield return new WaitForSeconds(1f);
        }
        private void SetNearstSurvivor() {
            if (destinations.Count > 0 && player != null) {
                nearstSurvivor = destinations[0];
                foreach (Transform t in destinations) {
                    if (Vector3.Distance(t.position, player.transform.position) < Vector3.Distance(nearstSurvivor.position, player.transform.position)) {
                        nearstSurvivor = t;
                    }
                }
                Vector3 fromSurvivor = nearstSurvivor.position;
                fromSurvivor.y += 10;
                line.SetPosition(0, fromSurvivor);
            }
        }
        #endregion
    }
}