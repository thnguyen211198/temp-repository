using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FastBoyFPS.Controller;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Journey : OverridableMonoBehaviour {
        [Header("References: ")]
        [SerializeField] private LineRenderer line;
        [SerializeField] private Transform[] paths;
        [SerializeField] private List<int> marked = new List<int>();
        [SerializeField] private Transform player;

        [Header("Settings: ")]
        [SerializeField] private bool build;
        [SerializeField] private float reachDistance;


        #region Methods
        private void OnDrawGizmos() {
            if (build) BuildPath();
        }
        private void Start() {
            player = FirstPersonController.Player.transform;
        }
        public override void OnUpdate() {
            if (player != null) {
                Vector3 rootPosition = player.position;
                rootPosition.y = line.GetPosition(line.positionCount - 1).y;
                line.SetPosition(line.positionCount - 1, rootPosition);
                if (line.positionCount > 2) {
                    Vector3 nextPoint = line.GetPosition(line.positionCount - 2);
                    if (Vector3.Distance(rootPosition, nextPoint) < reachDistance) {
                        line.positionCount--;
                    }
                }
            }
        }

        public void BuildPath() {
            line = GetComponent<LineRenderer>();    
            IEnumerable<Vector3> points = new Vector3[] { };
            for (int i = paths.Length - 1; i >= 0; i--) {
                IEnumerable<Vector3> pointI = paths[i].GetComponentsInChildren<Transform>().Select(t => t.position).Skip(1).Reverse();
                points = points.Concat(pointI);
            }
            line.positionCount = points.Count();
            line.SetPositions(points.ToArray());

            marked.Clear();
            int tmp = 0;
            foreach (Transform p in paths) {
                int lengthI = p.GetComponentsInChildren<Transform>().Select(t => t.position).Skip(1).Count();
                marked.Add(tmp + lengthI / 2);
                tmp += lengthI;
            }
        }
        #endregion
    }
}