using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Cuttable : MonoBehaviour {
        [SerializeField] private int damage = 50;
        #region Properties
        public EntityBase Vimtim { get; set; }
        public int Damage => damage;
        #endregion

        #region Methods
        public void Attack() {
            Vimtim.TakeDamage(damage);
        }
        #endregion
    }
}