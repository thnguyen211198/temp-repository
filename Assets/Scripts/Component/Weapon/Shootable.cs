using System;
using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Entity;
using FastBoyFPS.Utils;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Shootable : Attackable {
        private int damage = 100;
        private float shootDelay = 0.3f;
        private float lastShootTime = 0;
        private int bitMask = (1 << 3) | (1 << 6);
        #region Properties
        public bool CanShoot => Time.time > lastShootTime + shootDelay;
        #endregion

        #region Methods
        private void Start() {
        }
        public void Shoot(Ray ray) {
            //CustomizedDebug.Log("Bit mask: " + Convert.ToString(bitMask, 2).PadLeft(32, '0'));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, bitMask)) {
                //CustomizedDebug.Log("Raycast hit " + hit.collider.name);
                if (hit.transform.CompareTag("Entity")) {
                    hit.transform.GetComponent<EntityBase>().TakeDamage(hit.collider, damage);
                }
            }
            lastShootTime = Time.time;
            //Debug.DrawLine(ray.origin, ray.origin + ray.direction * 10, Color.red);
        }
        #endregion
    }
}