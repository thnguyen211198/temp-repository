using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Throwable : MonoBehaviour {
        [SerializeField] GameObject grenade;
        [SerializeField] Transform throwPoint;
        [SerializeField] float thrust = 30f;
        public void Throw() {
            GameObject g = Instantiate(grenade, throwPoint.position, throwPoint.rotation);
            Vector3 direction = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)).direction;
            g.GetComponent<Rigidbody>().AddForce(thrust * direction);
        }
    }
}