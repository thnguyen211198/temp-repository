using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Attackable : MonoBehaviour, IWeapon {
        public virtual void Attack() {

        }
    }
}