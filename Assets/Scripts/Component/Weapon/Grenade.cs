using System.Collections;
using System.Collections.Generic;
using FastBoyFPS.Entity;
using UnityEngine;

namespace FastBoyFPS.Component {
    public class Grenade : MonoBehaviour {
        #region Methods
        [SerializeField] ParticleSystem[] particles;
        [SerializeField] int damage = 900;
        [SerializeField] float explosionTime = 1000f;
        [SerializeField] float radius = 5f;
        private void Start() {
            StartCoroutine(CountExplosionTime());
        }
        private IEnumerator CountExplosionTime() {
            yield return new WaitForSeconds(explosionTime);
            foreach(ParticleSystem p in particles) {
                p.Play();
            }
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hitCollider in hitColliders) {
                EntityBase entity = hitCollider.GetComponent<EntityBase>();
                if (entity != null) {
                    entity.TakeDamage(damage);
                }
            }
        }

        #endregion
    }

}